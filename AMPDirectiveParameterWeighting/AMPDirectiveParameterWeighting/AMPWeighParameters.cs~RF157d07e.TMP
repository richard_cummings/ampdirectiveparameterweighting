﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;

namespace AMPDirectiveParameterWeighting
{
    /// <summary>
    /// Contains function returning AMP  Direcitive Parameters Weight
    /// </summary>
    public class AMPWeighParameters
    {
        public static int WeighDirectiveParameters(DataRow directivesRow)
        {
            int weight = 0;
            string apptType = directivesRow["AppointmentType"].ToString();
            string serviceProvider = directivesRow["ServiceProvider"].ToString();
            string acctLocation = directivesRow["AccountLocation"].ToString();

            if (apptType.Equals("ALL") && serviceProvider.Equals("ALL") && acctLocation.Equals("ALL"))
            {
                weight = 10;
            }
            else
            {
                if (!(apptType.Equals("ALL") || String.IsNullOrWhiteSpace(apptType)))
                {
                    weight += 4;
                }

                if (!(serviceProvider.Equals("ALL") || String.IsNullOrWhiteSpace(serviceProvider)))
                {
                    weight += 2;
                }

                if (!(acctLocation.Equals("ALL") || String.IsNullOrWhiteSpace(acctLocation)))
                {
                    weight += 1;
                }
            }

            return weight;
        }

        /// <summary>
        /// Takes a DataRow of AMP Directive Parameters and returns its weight (priority).
        /// </summary>
        /// <param name="directivesRow"> Row from AMP Directive Parameters table </param>
        /// <returns> weight </returns>
        public static int WeighParameters(DataRow directivesRow)
        {
            try
            {
                // Get weights from config file
                int acctLocationWeight = Convert.ToInt32(WebConfigurationManager.AppSettings["AccountLocation"]);
                int serviceProviderWeight = Convert.ToInt32(WebConfigurationManager.AppSettings["ServiceProvider"]);
                int apptTypeWeight = Convert.ToInt32(WebConfigurationManager.AppSettings["AppointmentType"]);
                int weight = 0;
                
                string apptType = directivesRow["AppointmentType"].ToString();
                string serviceProvider = directivesRow["ServiceProvider"].ToString();
                string acctLocation = directivesRow["AccountLocation"].ToString();

                // Default "ALL" parameters are to be given highest priority
                if (apptType.Equals("ALL") && serviceProvider.Equals("ALL") && acctLocation.Equals("ALL"))
                {
                    weight = Int32.MaxValue;
                }
                else
                {
                    if (!(apptType.Equals("ALL") || String.IsNullOrWhiteSpace(apptType)))
                    {
                        weight += apptTypeWeight;
                    }

                    if (!(serviceProvider.Equals("ALL") || String.IsNullOrWhiteSpace(serviceProvider)))
                    {
                        weight += serviceProviderWeight;
                    }

                    if (!(acctLocation.Equals("ALL") || String.IsNullOrWhiteSpace(acctLocation)))
                    {
                        weight += acctLocationWeight;
                    }
                }

                return weight;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex);
            }

            return 0;
        }

        /// <summary>
        /// Takes a DataRow of AMP Directive Parameters and returns its weight (priority).
        /// </summary>
        /// <param name="row"> Row from AMP Directive Parameters table </param>
        /// <returns> weight </returns>
        public static int DirectivesWeight(DataRow row)
        {
            var appSettings = WebConfigurationManager.AppSettings;
            int weight = 0;

            Dictionary<string, int> directiveWeights = new Dictionary<string, int>();

            try
            {
                // Add all AMP app settings from config file to dictionary
                foreach (var key in appSettings.AllKeys)
                {
                    if (Regex.IsMatch(key, ("AMP*")))
                    {
                        // if column exists in table, add to directiveWeights
                        int value;
                        bool result = Int32.TryParse(appSettings[key], out value);
                        if (row.Table.Columns.Contains(key.Remove(0, 3)))
                        {
                            directiveWeights.Add(key.Remove(0, 3), value);
                        }
                    }
                }

                // If every entry in row contains default value ("ALL") return with default weight setting
                int count = 0;

                foreach (string s in directiveWeights.Keys)
                {
                    if (!row[s].Equals("ALL"))
                    {
                        break;
                    }
                    else
                    {
                        count++;
                    }
                }

                if (count == directiveWeights.Keys.Count)
                {
                    return Convert.ToInt32(appSettings["DefaultWeight"]);
                }

                // Add value to weight if column is not default or null
                foreach (KeyValuePair<string, int> entry in directiveWeights)
                {
                    if (row[entry.Key] != null && !row[entry.Key].Equals("ALL"))
                    {
                        weight += entry.Value;
                    }
                }

                return weight;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception Occured: {0}", ex);
            }
            return 0;
        }
    }
}
