﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;

namespace AMPDirectiveParameterWeighting
{
    /// <summary>
    /// Contains functions returning AMP  Direcitive Parameters Weight
    /// </summary>
    public class AMPWeighParameters
    {
        /// <summary>
        /// Takes a DataRow of AMP Directive Parameters and returns its weight (priority).
        /// </summary>
        /// <param name="row"> Row from AMP Directive Parameters table </param>
        /// <returns> weight </returns>
        public static int WeighDirectivesRow(DataRow row)
        {
            var appSettings = WebConfigurationManager.AppSettings;
            int weight = 0;

            Dictionary<string, int> directiveWeights = new Dictionary<string, int>();

            try
            {
                // Add all AMP app settings from config file to dictionary
                foreach (var key in appSettings.AllKeys)
                {
                    if (Regex.IsMatch(key, ("AMP*")))
                    {
                        // if column exists in table, add to directiveWeights
                        int value;
                        // remove 'AMP' to get column name
                        string columnName = key.Remove(0, 3);
                        bool result = Int32.TryParse(appSettings[key], out value);
                        if (row.Table.Columns.Contains(columnName) && !string.IsNullOrWhiteSpace(row[columnName].ToString()))
                        {
                            directiveWeights.Add(key.Remove(0, 3), value);
                        }
                    }
                }

                // If every entry in row contains default value ("ALL") return with default weight setting
                int count = 0;

                foreach (string column in directiveWeights.Keys)
                {
                    if (!row[column].Equals("ALL"))
                    {
                        break;
                    }
                    else
                    {
                        count++;
                    }
                }

                if (count == directiveWeights.Keys.Count)
                {
                    return Convert.ToInt32(appSettings["DefaultWeight"]);
                }

                // Add value to weight if column is not default or null
                foreach (KeyValuePair<string, int> entry in directiveWeights)
                {
                    if (row[entry.Key] != null && !row[entry.Key].Equals("ALL"))
                    {
                        weight += entry.Value;
                    }
                }

                return weight;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception Occured: {0}", ex);
            }
            return 0;
        }

        /// <summary>
        /// Returns weight of JSON object passed as string.
        /// </summary>
        /// <param name="directives"> JSON object containing Directive Parameters information. </param>
        /// <returns> weight </returns>
        public static int WeighDirectives(string directives)
        {
            // Get weight values from config file, value will be 0 if cannot parse to int
            var appSettings = WebConfigurationManager.AppSettings;
            int acctLocationWeight, serviceProviderWeight, apptTypeWeight, defaultWeight;
            bool result = Int32.TryParse(appSettings["AMPAccountLocation"], out acctLocationWeight);
            result = Int32.TryParse(appSettings["AMPServiceProvider"], out serviceProviderWeight);
            result = Int32.TryParse(appSettings["AMPAppointmentType"], out apptTypeWeight);
            result = Int32.TryParse(appSettings["DefaultWeight"], out defaultWeight);

            // weight to return
            int weight = 0;

            try
            {
                // validate JSON object against defined schema
                string schemaJson = @"{""Parameter"":{'type':'string'},""Parameter"":{'type':'string'},""Parameter"":{'type':'string'}}";
                JsonSchema schema = JsonSchema.Parse(schemaJson);
                JObject jDirectives = JObject.Parse(directives);

                bool valid = jDirectives.IsValid(schema);

                if (valid)
                {
                    // Deserialize string into Dictionary
                    Dictionary<string, string> dictDirectives = JsonConvert.DeserializeObject<Dictionary<string, string>>(directives);

                    // If every entry in row contains default value ("ALL") return with default weight setting
                    int count = 0;

                    foreach (string param in dictDirectives.Values)
                    {
                        if (!param.Equals("ALL"))
                        {
                            break;
                        }
                        else
                        {
                            count++;
                        }
                    }

                    if (count == dictDirectives.Keys.Count)
                    {
                        return defaultWeight;
                    }

                    // Add parameter weights to 'weight' if they were passed in directives.
                    foreach (KeyValuePair<string, string> entry in dictDirectives)
                    {
                        if (!string.IsNullOrWhiteSpace(entry.Value) && !string.IsNullOrWhiteSpace(entry.Key) && !entry.Key.Equals("ALL"))
                        {
                            if (entry.Key.Equals("AccountLocation"))
                            {
                                weight += acctLocationWeight;
                            }
                            else if (entry.Key.Equals("ServiceProvider"))
                            {
                                weight += serviceProviderWeight;
                            }
                            else if (entry.Key.Equals("AppointmentType"))
                            {
                                weight += apptTypeWeight;
                            }
                        }
                    }

                    return weight;
                }
                else
                {
                    return 0;
                }
            }
            catch (JsonReaderException ex)
            {
                Console.WriteLine("Invalid JSON format for deserializing. Error: {0}", ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception Occured: {0}", ex);
            }
            
            return 0;
        }

        /// <summary>
        /// Return weight of boolean values.
        /// </summary>
        /// <param name="acctLocation"> was AccountLocation user defined? </param>
        /// <param name="serviceProvider"> was ServiceProvider user defined? </param>
        /// <param name="apptType"> was AppointmentType user defined? </param>
        /// <param name="defaultSetup"> was created with default setup? </param>
        /// <returns> weight </returns>
        public static int WeighDirectives(bool acctLocation, bool serviceProvider, bool apptType, bool defaultSetup)
        {
            // Get weight values from config file, value will be 0 if cannot parse to int
            var appSettings = WebConfigurationManager.AppSettings;
            int acctLocationWeight, serviceProviderWeight, apptTypeWeight, defaultWeight;
            bool result = Int32.TryParse(appSettings["AMPAccountLocation"], out acctLocationWeight);
            result = Int32.TryParse(appSettings["AMPServiceProvider"], out serviceProviderWeight);
            result = Int32.TryParse(appSettings["AMPAppointmentType"], out apptTypeWeight);
            result = Int32.TryParse(appSettings["DefaultWeight"], out defaultWeight);

            // weight to return
            int weight = 0;

            if (defaultSetup)
            {
                return defaultWeight;
            }
            else
            {
                if (acctLocation)
                    weight += acctLocationWeight;
                if (serviceProvider)
                    weight += serviceProviderWeight;
                if (apptType)
                    weight += apptTypeWeight;
            }
            return weight;
        }
    }
}
