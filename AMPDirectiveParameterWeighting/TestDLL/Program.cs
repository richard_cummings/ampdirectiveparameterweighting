﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.PromptAlert.Data;
using System.Data;
using AMPDirectiveParameterWeighting;

namespace TestDLL
{
    class Program
    {
        public static void WeighRowsDict()
        {
            SqlCommand dataCommand = null;

            using (SqlConnection conn = new SqlConnection("server=10.76.2.8;database=promptalert_test;user id=promptdbuser; password=orange77!"))
            {
                try
                {
                    conn.Open();

                    dataCommand = CommonData.BuildSqlCommand("usp_PA_ADMIN_AMPDirectiveParametersTest", conn);
                    SqlDataAdapter da = new SqlDataAdapter(dataCommand);
                    DataSet dsDirectiveParams = new DataSet();
                    da.Fill(dsDirectiveParams, "TestAMPDirectiveParameters");
                    DataTable dtDirectiveParams = dsDirectiveParams.Tables["TestAMPDirectiveParameters"];

                    foreach (DataRow row in dtDirectiveParams.Rows)
                    {
                        Console.WriteLine("Row {0}: Weight {1}", row[0], AMPWeighParameters.WeighDirectivesRow(row));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fatal Exception {0}", ex);
                }
            }
        }

        public static void WeighRows()
        {
            string json = @"{""AccountLocation"":""Toronto"",""ServiceProvider"":""Dr. Alex"", ""AppointmentType"":""Imaging Dept.""}";
            string json1 = @"{""AccountLocation"":""Toronto"",""ServiceProvider"":""Dr. Alex""}";
            string json2 = @"{""AppointmentType"":""Imaging Dept.""}";
            string json3 = @"{""AccountLocation"":""Toronto"",""AppointmentType"":""Imaging Dept.""}";
            string json4 = @"{""ServiceProvider"":""Dr. Alex"", ""AppointmentType"":""Imaging Dept.""}";
            string json5 = @"{""AccountLocation"":""ALL"",""ServiceProvider"":""ALL"", ""AppointmentType"":""ALL""}";
            string json6 = "Testing";

            Console.WriteLine("Weight of: {0}", AMPWeighParameters.WeighDirectives(json));
            Console.WriteLine("Weight of: {0}", AMPWeighParameters.WeighDirectives(json1));
            Console.WriteLine("Weight of: {0}", AMPWeighParameters.WeighDirectives(json2));
            Console.WriteLine("Weight of: {0}", AMPWeighParameters.WeighDirectives(json3));
            Console.WriteLine("Weight of: {0}", AMPWeighParameters.WeighDirectives(json4));
            Console.WriteLine("Weight of: {0}", AMPWeighParameters.WeighDirectives(json5));
            Console.WriteLine("Weight of: {0}", AMPWeighParameters.WeighDirectives(json6));
        }

        public static void WeighRowsBool()
        {
            Console.WriteLine("Weight: {0}", AMPWeighParameters.WeighDirectives(true, true, true, false));
            Console.WriteLine("Weight: {0}", AMPWeighParameters.WeighDirectives(false, false, true, false));
            Console.WriteLine("Weight: {0}", AMPWeighParameters.WeighDirectives(false, false, false, true));
            Console.WriteLine("Weight: {0}", AMPWeighParameters.WeighDirectives(true, true, false, false));
            Console.WriteLine("Weight: {0}", AMPWeighParameters.WeighDirectives(true, false, true, false));
        }

        static void Main(string[] args)
        {
            //WeighRowsDict();
            WeighRows();
            //WeighRowsBool();
            Console.ReadLine();
        }
    }
}
